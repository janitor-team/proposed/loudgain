Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: loudgain
Upstream-Contact: Matthias C. Hormann <mhormann@gmx.de>
Source: https://github.com/Moonbase59/loudgain
License: BSD-2-Clause
Files-Excluded: bin/loudgain* debian
Comment: The files matching these paths have been removed because we do not
         want to import the upstream Debian directory, a static binary of
         loudgain or the outdated upstream Debian binary.

Files: *
Copyright: 2014 Alessandro Ghedini <alessandro@ghedini.me>
           2019 Matthias C. Hormann <mhormann@gmx.de>
License: BSD-2-Clause

Files: cmake_uninstall.cmake.in config.h.in
       bin/rgbpm
       docs/images/*
Copyright: 2019 Matthias C. Hormann <mhormann@gmx.de>
License: BSD-2-Clause

Files: debian/*
Copyright: 2020 Hugh McMaster <hugh.mcmaster@outlook.com>
License: GPL-2+

License: BSD-2-Clause
 Copyright (c) 2014, Matthias C. Hormann <mhormann@gmx.de>
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
 .
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 .
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 .
  You should have received a copy of the GNU General Public License
  along with this program;  if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
  MA 02110-1301, USA.
 .
  On Debian GNU/Linux systems, the complete text of the GNU General
  Public License version 2 can be found in
  `/usr/share/common-licenses/GPL-2'.
